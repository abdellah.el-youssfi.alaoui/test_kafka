import sys
from json import dumps

from kafka import KafkaProducer

kafkaBrokers = 'kafka-broker-recette-1:9094,kafka-broker-recette-2:9094,kafka-broker-recette-3:9094'
caRootLocation = 'CARoot.pem'
certLocation = 'certificate.pem'
keyLocation = 'key.pem'
topic = 'ACTIVATION_DOTATION_RECETTE'
password = 'sslpassphrase'
userName = 'kafkabroker'
saslPassword = 'kafkabroker-secret'
saslMechanism = 'PLAIN'
securityProtocol = 'SASL_SSL'

producer = KafkaProducer(bootstrap_servers=kafkaBrokers,
                         security_protocol=securityProtocol,
                         ssl_check_hostname=True,
                         ssl_cafile=caRootLocation,
                         ssl_certfile=certLocation,
                         ssl_keyfile=keyLocation,
                         ssl_password=password,
                         sasl_plain_username=userName,
                         sasl_mechanism=saslMechanism,
                         sasl_plain_password=saslPassword)


def send_message(message):
    try:
        producer.send(topic, dumps(message).encode('utf-8'))
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise


if __name__ == '__main__':
    message = {"eventType": "RpaActivationDotationCommand", "eventId": "test-kafka-python",
               "correlationId": "test-kafka-python", "customerId": "dummy_value", "cin": "AA24337",
               "firstName": "user-ecom-{}".format("dummy_value"),
               "lastName": "user-ecom-{}".format("dummy_value"), "amount": None}
    send_message(message)

import string
import time
import typing
from json import dumps
import random
import certifi

from confluent_kafka import Producer
import logging

from data.touristique import touristique_data
from data.ecom import ecom_data
from data.webpay import webpay_data

kafka_config = {
    'bootstrap.servers': '172.29.5.231:9094,172.29.5.232:9094,172.29.5.234:9094',
    'ssl.ca.location': './ca-cert',
    'security.protocol': 'SASL_SSL',
    'sasl.mechanism': 'PLAIN',
    'sasl.username': 'kafkabroker',
    'sasl.password': 'kafkabroker-secret'
}


def produce_message(key: typing.Dict, json_message: typing.Dict, topic, headers):
    producer = Producer(kafka_config)

    try:
        # logging.warning("producing a new message to topic ACTIVATION_DOTATION_RECETTE ")
        logging.info("producing message:{}".format(json_message))
        producer.produce(topic, value=dumps(json_message).encode('utf-8'),
                         key=bytes(dumps(key), encoding='utf-8'),
                         headers=headers)
        producer.flush()
    except Exception as e:
        logging.error(e)


def get_random_string(length):
    letters = string.ascii_lowercase
    random_string = ''.join(random.choice(letters) for i in range(length))
    return random_string


def get_new_ecom_message():
    customer_id = get_random_string(10)
    event_id = get_random_string(10)
    return {"eventType": "RpaActivationDotationCommand", "eventId": event_id,
            "correlationId": "ecome-correlationId", "customerId": customer_id, "cin": "AA24337",
            "firstName": "user-ecom-{}".format(event_id),
            "lastName": "user-ecom-{}".format(event_id), "amount": None}


def get_new_touristique_message():
    customer_id = get_random_string(10)
    event_id = get_random_string(10)
    return {"eventType": "RpaActivationDotationCommand", "eventId": event_id,
            "correlationId": "5df0cec352607621", "customerId": customer_id, "cin": "AA24337",
            "firstName": "user-tour-{}".format(event_id),
            "lastName": "user-tour-{}".format(event_id), "amount": 200}


def produce_x_touristique_message(x):
    topic = 'ACTIVATION_TOURISTIQUE_RECETTE02'
    headers = [('x-type', 'RpaActivationDotationCommand')]
    message = {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": 0,
        "correlationId": "recette",
        "customerId": "fixed-12",
        "cin": "BE790483",
        "firstName": "KARIM",
        "lastName": "ALAMI",
        "amount": 100
    }
    for i in range(1, x):
        message['eventId'] = i
        message['customerId'] = get_random_string(10)
        produce_message({"customerId": message['customerId']}, message, topic, headers)
        time.sleep(1)


def producer_touristique():
    topic = 'ACTIVATION_TOURISTIQUE_RECETTE02'
    headers = [('x-type', 'RpaActivationDotationCommand')]
    for request in touristique_data:
        produce_message({"customerId": request.get("customerId")}, request, topic, headers)
        time.sleep(1)
        logging.info("sleeping for 1 seconds ..")


def producer_ecom():
    topic = 'ACTIVATION_DOTATION_RECETTE'
    headers = [('x-type', 'RpaActivationDotationCommand')]
    for request in ecom_data:
        produce_message({"customerId": request.get("customerId")}, request, topic, headers)
        time.sleep(1)
        logging.info("sleeping for 1 seconds ..")


def producer_webpay():
    topic = 'ACTIVATION_WEBPAY_RECETTE'
    headers = [('x-type', 'RPA_WEBPAY_COMMAND')]
    for request in webpay_data:
        produce_message({"customerId": request.get("customerId")}, request, topic, headers)
        time.sleep(1)
        logging.info("sleeping for 1 seconds ..")


def produce_ecom_and_touristique():
    topic_ecom = 'ACTIVATION_DOTATION_RECETTE'
    topic_tour = 'ACTIVATION_TOURISTIQUE_RECETTE'
    for i in range(100):
        if i % 2 == 0:
            message = get_new_ecom_message()
            produce_message({"customerId": message.get("customerId")}, message, topic_ecom)
        else:
            message = get_new_touristique_message()
            produce_message({"customerId": message.get("customerId")}, message, topic_tour)


if __name__ == '__main__':
    logging.info("start producer..")
    # produce_ecom_and_touristique()
    # producer_ecom()
    # producer_touristique()
    produce_x_touristique_message(12)

import time

from confluent_kafka import Producer, Consumer
import logging

def test_consumer():
    consumer_config = {
        'bootstrap.servers': 'kafka-broker-recette-1:9094,kafka-broker-recette-2:9094,kafka-broker-recette-3:9094',
        'ssl.ca.location': './ca-cert',
        'security.protocol': 'SASL_SSL',
        'sasl.mechanism': 'PLAIN',
        'sasl.username': 'kafkabroker',
        'sasl.password': 'kafkabroker-secret',
        'auto.offset.reset': 'earliest',
        'group.id': 'test234567898765',
        'enable.auto.commit': False,
        }

    consumer = Consumer(consumer_config)
    consumer.subscribe(['ACTIVATION_DOTATION_RECETTE'])
    counter = 0
    while counter < 1:
        time.sleep(3)
        logging.warning("sleeping for 3 seconds")
        message = consumer.poll(5.0)
        if message is None:
            logging.warning("the message is none")
        else:
            if message.error():
                logging.warning("inside the message error")
                logging.error('message error {}'.format(message.error()))
            else:
                logging.warning("inside the message value")
                logging.warning("the content of the message is {}".format(message.value()))


if __name__ == '__main__':
    logging.warning("start consumer")
    test_consumer()

import string
import random


def get_random_string(length):
    letters = string.ascii_lowercase
    random_string = ''.join(random.choice(letters) for i in range(length))
    return random_string
touristique_data = [
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-12",
        "cin": "BE790483",
        "firstName": "KARIM",
        "lastName": "ALAMI",
        "amount": 5000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-564",
        "cin": "BL0000028",
        "firstName": "MOULAY-HAFID",
        "lastName": "ALAOUI",
        "amount": 5000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-4543",
        "cin": "BN87678",
        "firstName": "OTH.MANE",
        "lastName": "BEN.ROUYNE",
        "amount": 5000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-09098",
        "cin": "AA25228",
        "firstName": "M'HEDI",
        "lastName": "AR'BI",
        "amount": 5000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-4532",
        "cin": "AA25559",
        "firstName": "RA_BI",
        "lastName": "ZIDAN_E",
        "amount": 5000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-54432233",
        "cin": "AA25559",
        "firstName": "R",
        "lastName": "Z",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-9898",
        "cin": "AA25559",
        "firstName": "RR",
        "lastName": "ZZ",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-878787878",
        "cin": "AB06143B",
        "firstName": "MEHDI",
        "lastName": "CININCORECT",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-9898989",
        "cin": "AA27229",
        "firstName": "SANSSOLDE",
        "lastName": "SANSSOLDE",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-9998877",
        "cin": "AA55669",
        "firstName": "FATIMA",
        "lastName": "SANOM",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-3421",
        "cin": "AA55669",
        "firstName": "FATIMA",
        "lastName": "SANOM",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-1234322",
        "cin": "DC4488",
        "firstName": "JAMALI",
        "lastName": "HASSAN",
        "amount": 1010
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-1234543",
        "cin": "RZ234",
        "firstName": "NAJM",
        "lastName": "SALAH",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-3241",
        "cin": "BL122559",
        "firstName": "RACHIDDD",
        "lastName": "POINTTT",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-3241",
        "cin": "BL000001",
        "firstName": "MEHDI",
        "lastName": "UNELET",
        "amount": 1000
    },
    {
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-12343",
        "cin": "AA20999",
        "firstName": "KARIM",
        "lastName": "TIRET",
        "amount": 1000
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-45455554",
        "cin": "DC32116",
        "firstName": "Soufiane",
        "lastName": "Boufal",
        "amount": 40000
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-5432332",
        "cin": "BF32122",
        "firstName": "rachid",
        "lastName": "sanmontant",
        "amount": None
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-544543339",
        "cin": "BG34322",
        "firstName": "hamid",
        "lastName": "negatif",
        "amount": -1000
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-54434229",
        "cin": "BH2134",
        "firstName": "Mehdi",
        "lastName": "Multiplecinq",
        "amount": 1025
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-88878",
        "cin": "BJ65454",
        "firstName": "Yassine",
        "lastName": "virgule",
        "amount": 1000.10
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-778780",
        "cin": "BK43243",
        "firstName": "Zineb",
        "lastName": "zero",
        "amount": 0
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-778780",
        "cin": "AP25559",
        "firstName": "R",
        "lastName": "Z",
        "amount": 10000
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-778780",
        "cin": "AP25559",
        "firstName": "RR",
        "lastName": "ZZ",
        "amount": 10000
    },
{
        "eventType": "RpaActivationTouristiqueCommand",
        "eventId": "recette-{}".format(get_random_string(10)),
        "correlationId": "recette",
        "customerId": "fixed-778780",
        "cin": "AA67667",
        "firstName": "",
        "lastName": "",
        "amount": None
    }
]


from confluent_kafka import Producer, Consumer
import logging

kafka_config = {
    'bootstrap.servers': '172.29.5.231:9094,172.29.5.232:9094,172.29.5.234:9094',
    'ssl.ca.location': './ca-cert',
    'security.protocol': 'SASL_SSL',
    'sasl.mechanism': 'PLAIN',
    'sasl.username': 'kafkabroker',
    'sasl.password': 'kafkabroker-secret'
    }


def test_producer():
    logging.warning("producing a new message to topic ACTIVATION_DOTATION_RECETTE ")
    producer = Producer(kafka_config)
    try:
        producer.produce('ACTIVATION_DOTATION_RECETTE', value="test value", key="test key",
                         headers=[('TO_RPA', 'FILTER')])
        producer.flush()
    except Exception as e:
        logging.error(e)


def test_consumer():
    consumer_config = {
        'bootstrap.servers': 'kafka-broker-recette-1:9094,kafka-broker-recette-2:9094,kafka-broker-recette-3:9094',
        'ssl.ca.location': './ca-cert',
        'security.protocol': 'SASL_SSL',
        'sasl.mechanism': 'PLAIN',
        'sasl.username': 'kafkabroker',
        'sasl.password': 'kafkabroker-secret',
        'auto.offset.reset': 'earliest',
        'group.id': 'test',
        'enable.auto.commit': False,
        }

    consumer = Consumer(consumer_config)
    consumer.subscribe(['ACTIVATION_DOTATION_RECETTE'])
    counter = 0
    while counter > 0:
        message = consumer.poll(5.0)
        if message is None:
            logging.warning("the message is none")
        else:
            if message.error():
                logging.warning("inside the message error")
                logging.error('message error {}'.format(message.error()))
            else:
                logging.warning("inside the message value")
                logging.warning("the content of the message is {}".format(message.value()))


if __name__ == '__main__':
    #logging.warning("trying to produce  message to ACTIVATION_DOTATION_RECETTE..")
    #test_producer()
    #time.sleep(2)
    #logging.warning("consuming messages from ACTIVATION_DOTATION_RECETTE ..")
    #test_consumer()
    while True:
        logging.info("keep running...")

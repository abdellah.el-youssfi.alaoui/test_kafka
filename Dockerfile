FROM python:3.9

WORKDIR /usr/src/app

ADD requirements.txt ./

RUN  export https_proxy=http://172.29.70.100:8081  && export http_proxy=http://172.29.70.100:8081 && pip install --no-cache-dir -r requirements.txt


ADD data/ecom.py ./data/ecom.py
ADD data/touristique.py ./data/touristique.py
ADD main.py ./
ADD kafka_producer.py ./
ADD kafka_consumer.py ./
ADD kafka_vente_producer.py ./
ADD ca-cert ./
ADD certs/certificate.pem ./
ADD certs/key.pem ./
ADD certs/CARoot.pem ./
ADD test_ssl_producer.py ./
ADD kafka_python_producer.py ./
ADD certs/cert.pem ./
ENV SSL_CERT_FILE=/usr/src/app/cert.pem
RUN echo "172.29.5.231 kafka-broker-recette-1" >> /etc/hosts
RUN echo "172.29.5.232 kafka-broker-recette-2" >> /etc/hosts
RUN echo "172.29.5.234 kafka-broker-recette-3" >> /etc/hosts
CMD [ "python", "main.py" ]
import string
import time
import typing
from json import dumps
import random
import certifi


from confluent_kafka import Producer
import logging

kafka_config = {
    'bootstrap.servers': '172.29.5.231:9094,172.29.5.232:9094,172.29.5.234:9094',
    'ssl.ca.location': './ca-cert',
    'ssl.certificate.location': './certificate.pem',
    'ssl.key.location': './key.pem',
    'ssl.key.password': 'sslpassphrase',
    'security.protocol': 'SASL_SSL',
    'sasl.mechanism': 'PLAIN',
    'sasl.username': 'kafkabroker',
    'sasl.password': 'kafkabroker-secret'
}


def get_random_string(length):
    letters = string.ascii_lowercase
    random_string = ''.join(random.choice(letters) for i in range(length))
    return random_string


def produce_message(key: typing.Dict, json_message: typing.Dict, topic):
    producer = Producer(kafka_config)

    try:
        # logging.warning("producing a new message to topic ACTIVATION_DOTATION_RECETTE ")
        logging.info("producing message:{}".format(json_message))
        producer.produce(topic, value=dumps(json_message).encode('utf-8'),
                         key=bytes(dumps(key), encoding='utf-8'),
                         headers=[('x-type', 'RpaActivationDotationCommand_bad')])
        producer.flush()
    except Exception as e:
        logging.error(e)

def get_new_ecom_message():
    customer_id = get_random_string(10)
    event_id = get_random_string(10)
    return {"eventType": "RpaActivationDotationCommand", "eventId": event_id,
            "correlationId": "ecome-correlationId", "customerId": customer_id, "cin": "AA24337",
            "firstName": "user-ecom-{}".format(event_id),
            "lastName": "user-ecom-{}".format(event_id), "amount": None}


def producer_ecom():
    topic = 'ACTIVATION_DOTATION_RECETTE'
    for i in range(5):
        request = get_new_ecom_message()
        produce_message({"customerId": request.get("customerId")}, request, topic)
        time.sleep(1)
        logging.info("sleeping for 1 seconds ..")



if __name__ == '__main__':
    logging.info("start producer..")
    logging.warning("start producer..")
    #producer_ecom()
    print(certifi.contents())

import time

from confluent_kafka import Producer, Consumer
import logging

kafka_config = {
    'bootstrap.servers': '172.29.5.231:9094,172.29.5.232:9094,172.29.5.234:9094',
    'ssl.ca.location': './ca-cert',
    'security.protocol': 'SASL_SSL',
    'sasl.mechanism': 'PLAIN',
    'sasl.username': 'kafkabroker',
    'sasl.password': 'kafkabroker-secret'
    }


def test_producer():

    producer = Producer(kafka_config)
    counter = 0
    while True:
        logging.warning("sleeping for 10 s")
        time.sleep(10)
        try:
            logging.warning("producing a new message to topic ACTIVATION_DOTATION_RECETTE ")
            counter = counter + 1
            producer.produce('ACTIVATION_DOTATION_RECETTE', value="new message #{}".format(counter), key="test key",
                             headers=[('TO_RPA', 'FILTER')])
            producer.flush()
        except Exception as e:
            logging.error(e)
if __name__ == '__main__':
    logging.warning("start producer..")
    test_producer()

